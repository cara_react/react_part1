# Partie I

Lien vers l’exercice : https://codepen.io/vlaude/pen/NOQmaJ?editors=0010

Lien le diapo ! https://docs.google.com/presentation/d/15t-IhCyhDDI-i6luThA5-PcmRXYgfhhSe3sSYIywrJ8/edit?usp=sharing

Lien vers le tuto React : https://reactjs.org/ 

Dans cette première partie, nous allons uniquement nous focaliser sur :

- L’injection de données entre composants
- L’affichage de composant

Nous allons commencer par afficher un simple plateau de jeu, affichant des cartes non mélangées et sans paire.

**Résultat attendue :**

![](images/Partie_I_resultat.PNG)

 

## Le component Application

Ce component est la racine de l’application. C’est lui qui va connaître l’état du plateau de jeu et des cartes. Il va orchestrer le comportement de l’application en notifiant les autres components des changements d’état de l’application (selection d’une carte, paire trouvée etc.).

![](images/constructor.png)


C’est le constructeur du component.

La ligne super(props) est **obligatoire** pour tout **component**.
On lui attribue comme état le **plateau (board)**, qui est une liste de 16 éléments initialisés à null.
La méthode initBoard initialise les cartes du plateau en leur attribuant une valeur.

![](images/Application_render.PNG)


La méthode **render** décrit l’affichage du component.

Ici on affiche le titre de l’application, ainsi qu’une instance du component **Memory**.

L’input board={ board } signifie qu’on **injecte** au component **Memory** un propriétée appelée board et ayant pour valeur this.state.board.


## Travail à faire :

Nous allons compléter les components **Card** et **Memory**.

1- Complétez la méthode render du component **Card**

![](images/rendercard.png)


**Nous vous avons épargnez l’écriture du style css des différents éléments.**

**-> Ajoutez la class ‘card’ à votre bouton pour le styliser.
N’oubliez pas d’y afficher la valeur de la carte !**

Rappel:
````html
<div>
	{ code javascript }
</div>
````

2- Complétez la méthode renderCard du component Memory

Rappel :

````
card : {
	index: number;
	value: number;
}
````

![](images/Memory_renderCard.PNG)


3- Compléter la méthode render du component Memory

On doit afficher 4 cartes par ligne, pour cela utilisez la méthode renderCard et la constante board (qui pointe vers l’état board de l’application).

N’oubliez pas d’afficher toutes les 16 cartes, et non pas 16 fois la même !

Rappel:

````html
<div>
	{ code javascript }
</div>
````

![](images/Memory_render.PNG)
